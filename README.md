# Zotero 7 常用插件资源文件

本仓库提供了一系列适用于 Zotero 7 的常用插件资源文件，旨在帮助用户更好地管理和使用 Zotero 进行文献管理。以下是各插件的简要介绍：

## 插件列表

### 1. Better BibTex for Zotero
**描述**: 该插件专为 LaTeX 用户设计，使 Zotero 在 LaTeX 环境中更加高效。它提供了更好的 BibTeX 支持，帮助用户更方便地导出和管理 BibTeX 文件。

### 2. Delitemwithatt
**描述**: 该插件允许用户在删除 Zotero 中的条目或集合时，自动删除相关的附件。这对于管理大量文献和附件的用户来说非常实用。

### 3. Jasminum
**描述**: 这是一个专门为中文用户设计的 Zotero 插件，用于从中国知网（CNKI）获取中文文献的元数据。它能够自动识别和提取中文文献的详细信息，极大地方便了中文文献的管理。

### 4. Zotero 7 Meet ZotFile
**描述**: ZotFile 是一个功能强大的 Zotero 插件，用于管理文献附件。它能够自动重命名、移动和附加 PDF（或其他文件）到 Zotero 条目中，并支持格式化元数据、PDF 预览、参考样式、标签等功能。

## 使用方法

1. 下载本仓库中的插件文件。
2. 打开 Zotero 软件。
3. 进入 Zotero 的插件管理界面，选择“从文件安装插件”。
4. 选择下载的插件文件进行安装。
5. 安装完成后，重启 Zotero 以激活插件。

## 注意事项

- 请确保 Zotero 版本为 7.0 或更高版本，以兼容这些插件。
- 安装插件前，建议备份 Zotero 数据，以防意外情况发生。

## 贡献

欢迎大家提交问题和建议，帮助改进这些插件。如果您有新的插件或改进建议，请通过 GitHub 的 Issues 或 Pull Requests 提交。

## 许可证

本仓库中的插件资源文件遵循各自的许可证。请在使用前仔细阅读各插件的许可证信息。

---

希望这些插件能够帮助您更高效地使用 Zotero 进行文献管理！